# An Optimization Approach for Sub-event Detection and Summarization in Twitter

The project is used to perform a sub-event detection in sets of crawled Tweets.
Although the current version works with json files as input, the procedure followed and the execution time allows the execution using a stream-like input.

The methodology that was followed is described in the following publication:

**Meladianos P., Xypolopoulos C., Nikolentzos G., Vazirgiannis M. (2018) An Optimization Approach for Sub-event Detection and Summarization in Twitter. In: Pasi G., Piwowarski B., Azzopardi L., Hanbury A. (eds) Advances in Information Retrieval. ECIR 2018. Lecture Notes in Computer Science, vol 10772. Springer, Cham** (Available here: https://link.springer.com/chapter/10.1007/978-3-319-76941-7_36)

## Getting Started

The code is developed using the Anaconda distribution of Python version 2.7.
The required libraries are included in the **_requirements.txt_** file.
To create a new conda environment that includes the required libraries the following command should be executed.

```
conda create -n environment_name
```

To activate the conda environment that was just created, the following command should be used.

```
source activate environment_name
```

To install the required packages inside the conda environment use the following command:

```
$ while read requirement; do conda install --yes $requirement; done < requirements.txt
```

You should also use the following commands in order to install the CVXPY module (http://www.cvxpy.org/install/index.html) .

```
conda install -c conda-forge lapack
conda install -c cvxgrp cvxpy
```

### Input data

Before starting, you should convert your data to the format that is used.
The tweets that will be used as input need to be in CSV format, using ',' as delimiter and '"' as quote char.
Every line should contain the timestamp and the text of the tweet following:

```
"timestamp1", "text1"
"timestamp2", "text2"
```

The tweets in each file should be in chronological order, using the provided timestamp.
The input files should be stored in the same directory.


### Training

Due to the need of a threshold value during the detection process, a training process has been also included.
This step is not mandatory as you could start by setting the threshold equals zero and then adjust it according to your required amount of information.
To perform an accurate training process, you will need a sufficient amount of training data of the same type of event.
Given those files as input, a threshold will be generated for each one of the files that will be used for the sub-event detection process.
The training is performed by executing the **_Training.py_** file, while declaring the arguments that are needed.

```
usage: Training.py [-h] [--duration DURATION] [--v]
                   train_input test_input output thresholds

positional arguments:
  train_input          path to the directory containing the files that will be
                       used for train
  test_input           path to the directory containing the files that need to
                       receive threshold value
  output               path to the directory where the results would be stored
  thresholds           path to the file where the predefined thresholds are
                       stored for each train file

optional arguments:
  -h, --help           show this help message and exit
  --duration DURATION  duration of the period set in minutes (default 1 min)
  --v                  increase output verbosity


```

The output will be stored in a file named **_training_results.txt_** that will be located in the output directory that was declared.
The file will be formated as follows:

```
event_file_name1	threshold_value1
event_file_name2	threshold_value2
```

If you want to see the fiting of the training process in your day you could use the **_-v_** argument.


### Executing

The execution of the sub-event detection and summarization process is performed through the **_main.py_** file.
Again, a couple of arguments need to be used:

```
usage: main.py [-h] [--output OUTPUT] [--threshold THRESHOLD]
               [--periods PERIODS] [--duration DURATION] [--urls] [--retweets]
               [--mentions] [--stemming] [--duplicates] [--burst] [--v]
               [--summary SUMMARY] [--sp SP] [--c C]
               input

positional arguments:
  input                 path to the file containing the tweets that will be
                        used as input

optional arguments:
  -h, --help            show this help message and exit
  --output OUTPUT       path to the directory where the results would be
                        stored
  --threshold THRESHOLD
                        the threshold that will be used to determine if a
                        period contains a sub-event
  --periods PERIODS     number of previous periods that are considered
                        (default 10)
  --duration DURATION   duration of the period set in minutes (default 1 min)
  --urls                do not remove urls during preprocessing
  --retweets            do not remove retweets during preprocessing
  --mentions            do not remove users' mentions during preprocessing
  --stemming            do not perform stemming during preprocessing
  --duplicates          do not remove duplicate tweets during preprocessing
  --burst               use the burst detector instead of the optimized method
  --v                   increase output verbosity
  --summary SUMMARY     number of tweets that the summary will contains
                        (default 2)
  --sp SP               number of previous periods where the reappearance of
                        an sub-event is considered duplicate (default 3)
  --c C                 cosine similarity threshold to compare the summaries
                        (default 0.6)


```

## Referenced Python Libraries 

1. Steven Diamond & Stephen Boyd (2016). CVXPY: A Python-Embedded Modeling Language for Convex Optimization. Journal of Machine Learning Research, 17, 1-5.
2. Bird, Steven, Edward Loper and Ewan Klein (2009). Natural Language Processing with Python.  O'Reilly Media Inc.
3. St�fan van der Walt, S. Chris Colbert and Ga�l Varoquaux. The NumPy Array: A Structure for Efficient Numerical Computation, Computing in Science & Engineering, 13, 22-30 (2011), DOI:10.1109/MCSE.2011.37 (publisher link)

