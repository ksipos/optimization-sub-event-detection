import csv
import os

import Settings
from io import open


class Reader:
    """Class that is used to read event files in a specified format"""

    def __init__(self):
        self.file_pointer = None
        self.load_files = True
        self.loaded_file_name = ""
        self.loaded_clear_name = "" # <---
        self.starting_timestamp = 0
        self.file_periods, self.starting_tweets = [], []

    def get_tweets(self, file_name):
        """ The method that should be used to retrieve the tweets of the next time period"""
        if self.loaded_file_name != file_name:
            self.loaded_file_name = file_name
            self.loaded_clear_name = file_name.split("/")[-1]
            self.load_files = os.stat(self.loaded_file_name).st_size < 524288000
            if self.load_files:  # Load files with size less than 500MB)
                self.load_file()
            else:
                self.starting_timestamp = Settings.EVENT_TIMESTAMPS.get(self.loaded_clear_name, [0, 0])[0]
                self.starting_tweets = []
                if self.file_pointer is not None:
                    self.file_pointer.close()
                self.file_pointer = open(file_name, 'rb')

        return self.next_period() if self.load_files else self.read_next()

    def next_period(self):
        """A method that is used to retrieve the next available period when a file is loaded in memory"""
        return self.file_periods.pop(0) if len(self.file_periods) != 0 else None

    def load_file(self):
        """Reads the specified file and returns a list with tweets for the next period or an empty list."""
        start_timestamp = Settings.EVENT_TIMESTAMPS.get(self.loaded_clear_name, [0, 0])[0]
        end_timestamp = Settings.EVENT_TIMESTAMPS.get(self.loaded_clear_name, [0, 0])[1]

        if end_timestamp != 0 and end_timestamp < start_timestamp:
            return None
        window_end_timestamp = start_timestamp + int(Settings.TIME_WINDOW)
        with open(self.loaded_file_name, 'rb') as f:
            reader = csv.reader(f)
            temp_tweets = []
            tweets = []
            for row in reader:
                if start_timestamp == 0:
                    start_timestamp = int(row[0])
                    window_end_timestamp = start_timestamp + int(Settings.TIME_WINDOW)
                if int(row[0]) < start_timestamp:
                    continue
                if int(row[0]) > window_end_timestamp:
                    tweets.append(temp_tweets)
                    temp_tweets = []
                    start_timestamp = window_end_timestamp
                    window_end_timestamp += Settings.TIME_WINDOW
                    while int(row[0]) > window_end_timestamp:
                        tweets.append(temp_tweets)
                        temp_tweets = []
                        start_timestamp = window_end_timestamp
                        window_end_timestamp += Settings.TIME_WINDOW
                if int(row[0]) > end_timestamp > 0:
                    break
                temp_tweets.append(row)
        if temp_tweets:
            tweets.append(temp_tweets)
        self.file_periods = tweets

    def read_next(self):
        """Reads the specified file and returns a list with tweets for the next period or an empty list."""
        tweets = self.starting_tweets
        self.starting_tweets = []
        event_end_timestamp = Settings.EVENT_TIMESTAMPS.get(self.loaded_clear_name, [0, 0])[1]
        window_end_timestamp = self.starting_timestamp + int(Settings.TIME_WINDOW)

        if len(tweets) != 0 and int(tweets[0][1]) > window_end_timestamp:
            tweets = []
        limit = int(tweets[0][1]) if len(tweets) == 1 else 0
        if 0 < event_end_timestamp <= max(self.starting_timestamp, limit) or self.file_pointer.closed:
            self.file_pointer.close()
            return None

        reader = csv.reader(self.file_pointer)
        eof = True
        for row in reader:
            if self.starting_timestamp == 0:
                self.starting_timestamp = int(row[0])
                window_end_timestamp = self.starting_timestamp + int(Settings.TIME_WINDOW)

            if int(row[0]) < self.starting_timestamp:
                continue

            if int(row[0]) > window_end_timestamp or (int(row[0]) > event_end_timestamp > 0):
                self.starting_tweets = [row]
                self.starting_timestamp = window_end_timestamp
                eof = False
                break
            tweets.append(row)
        if eof:
            self.file_pointer.close()
        return tweets

    @staticmethod
    def count_tweets(file_name):
        from Preprocessor import Preprocessor
        from csv import reader
        from time import time
        with open(file_name, 'rb') as f:
            for row in reader(f):
                Settings.EVENT_TIMESTAMPS[file_name] = [int(row[0]), int(round(time() * 1000))]
                break
        Settings.TIME_WINDOW = 1 * 60000
        reader = Reader()
        c = 0
        while True:
            tweets = reader.get_tweets(file_name)
            if tweets is None:
                break
            elif len(tweets) == 0:
                continue
            tweets, full_tweets, tweets_timestamps, vocabulary, timestamps = Preprocessor.preprocess(tweets)
            c += len(tweets)
        return c
