from Util import Util
from collections import Counter
import Settings
import math
import re


class SummaryWriter:
    """A class that is used to print the summary of a event"""
    def __init__(self):
        pass

    @classmethod
    def write_summary(cls, file_name, sentences, indexes):
        """A method that given a list of sentences and their indexes will write to the specified file the summary. To
        accomplish that, cosine similarity is employed to remove sentences that look the same as previous sentences"""
        sentences = [sentence.split("||")[0] for sentence in sentences]
        summary = [sentences[0]]
        indexes_set = set(indexes)
        for i in range(1, len(sentences)):
            intersection = set(range(indexes[i] - Settings.SUMMARY_PERIODS, indexes[i])).intersection(indexes_set)
            similarities = [cls.cosine_similarity(sentences[i], sentences[indexes.index(val)]) for val in intersection]
            if (similarities and max(similarities) <= Settings.SUMMARY_SIMILARITY) or not similarities:
                summary.append(sentences[i])
        if file_name is not None:
            f = open(file_name, 'w')
            f.write(Util.beauty_print(summary))
            f.close()
        else:
            return summary

    @classmethod
    def cosine_similarity(cls, sentence1, sentence2):
        """A method that is used to calculate the cosine similarity between two sentences"""
        vec1 = Counter(re.compile(r'\w+').findall(sentence1))
        vec2 = Counter(re.compile(r'\w+').findall(sentence2))
        intersection = set(vec1.keys()) & set(vec2.keys())
        numerator = sum([vec1[x] * vec2[x] for x in intersection])

        denominator1 = math.sqrt(sum([vec1[x] ** 2 for x in vec1.keys()]))
        denominator2 = math.sqrt(sum([vec2[x] ** 2 for x in vec2.keys()]))

        return float(numerator) / (denominator1 * denominator2) if (denominator1 * denominator2) != 0 else 0
