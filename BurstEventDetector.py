from Preprocessor import Preprocessor
from Util import Util
import Settings


class BurstEventDetector:
    """Event detector that determines if a period is a sub-event based on the number of tweets"""

    total_tweets = {}
    file_name = ""

    def __init__(self, tweets, file_name, printing=True, write=True):
        self.tweets, self.full_tweets, self.tweets_timestamps, self.vocabulary, self.timestamps = Preprocessor.preprocess(tweets)
        self.tweets_number = len(self.tweets)
        self.file_name = file_name
        self.write = write
        self.printing = printing
        BurstEventDetector.total_tweets[file_name] = BurstEventDetector.total_tweets.get(file_name, 0) + self.tweets_number
        BurstEventDetector.file_name = file_name

    def detect_event(self):
        """Method that is used to decide if the current period is a sub-event or not"""
        toPrint = ["Period starts: " + self.timestamps[0] + " Period ends: " + self.timestamps[1],
                   "Number of tweets: " + str(self.tweets_number),
                   "Vocabulary size: " + str(len(self.vocabulary))]

        is_event = self.contains_event()
        period_score = self.tweets_number
        toPrint += [" No summary during baseline"]
        toPrint += [" "] + ["Burst detector result: " + str(self.contains_event())]
        if self.printing:
            print Util.beauty_print(toPrint)
        if self.write:
            f = open(BurstEventDetector.file_name, 'a')
            f.write(Util.beauty_print(["Timestamp: " + ' '.join(self.timestamps)] + ["No summary during baseline"] +
                                      ["-------------------------------------------"] +
                                      ["Result: " + str(is_event)] +
                                      ["Score: " + str(period_score)]))
            f.close()
        return [self.timestamps, is_event, "No summary during baseline || " + str(self.tweets_number)]

    def contains_event(self):
        """Returns True if the number of tweets in this period is greater than the specified threshold"""
        return self.tweets_number >= Settings.BURST_DETECTION_THRESHOLD[self.file_name.split("/")[-1][:-3] + "csv"]
