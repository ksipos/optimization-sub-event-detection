from argparse import ArgumentParser
from os import listdir
from os.path import isdir, isfile, join
from sys import stderr

import numpy as np

from LinearModel import LinearModel
from ReadData import Reader


def train(train_tweets_number, test_tweets_number, train_thresholds, output, verbose):
    """Method that is used to execute the event detection procedure for training purposes"""
    lm = LinearModel()
    lm.fit(train_tweets_number, train_thresholds, order=2)

    lm.find_line(train_tweets_number, train_thresholds, order=2, plot=verbose)

    training_error = 0
    for i in range(len(train_tweets_number)):
        print train_thresholds[i], lm.predict(train_tweets_number[i]), train_thresholds[i] - lm.predict(
            train_tweets_number[i])
        training_error += abs(train_thresholds[i] - lm.predict(train_tweets_number[i]))

    predicted_thresholds = {test_file: lm.predict(tweets_number) for test_file, tweets_number in
                            test_tweets_number.iteritems()}
    if verbose:
        print "Correlation coefficient", np.corrcoef(np.array(train_tweets_number), np.array(train_thresholds))
        print "Slope, Intercept, Power", lm.get_parameters()
        print "Training error:", training_error

    with open(output + "/training_results.txt", 'w') as f:
        for k, v in predicted_thresholds.iteritems():
            if verbose:
                print k + '\t' + v
            f.write(k + '\t' + v + "\n")


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("train_input", help="path to the directory containing the files that will be used for train")
    parser.add_argument("test_input", help='path to the directory containing the files that need to receive threshold '
                                           'value')
    parser.add_argument("output", help="path to the directory where the results would be stored")
    parser.add_argument("thresholds",
                        help="path to the file where the predefined thresholds are stored for each train file")
    parser.add_argument("--duration", help="duration of the period set in minutes (default 1 min)", default=1, type=float)
    parser.add_argument("--v", help="increase output verbosity", action="store_true")
    return parser.parse_args()


def argument_check():
    args = parse_arguments()
    if not isdir(args.train_input):
        stderr.write('The specified parameter should be a directory name, not a file.')
        raise SystemExit(1)
    elif not isdir(args.test_input):
        stderr.write('The specified parameter should be a directory name, not a file.')
        raise SystemExit(1)
    elif not isdir(args.output):
        stderr.write('The specified parameter should be a directory name, not a file.')
        raise SystemExit(1)
    elif not isfile(args.thresholds):
        stderr.write('The specified input file ' + str(args.thresholds) + ' was not found.')
        raise SystemExit(1)
    elif args.duration <= 0:
        stderr.write('The duration of the period should be a non-negative number.')
        raise SystemExit(1)
    return args


def get_thresholds(f_name, full_names):
    th = {}
    with open(f_name, 'r') as f:
        for line in f:
            name, threshold = line.split()
            th[[n for n in full_names if name in n][0]] = float(threshold)
    return th


if __name__ == "__main__":
    args = argument_check()
    import Settings

    Settings.TIME_WINDOW = args.duration * 60000
    training_files = [join(args.train_input, f) for f in listdir(args.train_input) if isfile(join(args.train_input, f))]
    test_files = [join(args.test_input, f) for f in listdir(args.test_input) if isfile(join(args.test_input, f))]
    thresholds = get_thresholds(args.thresholds, training_files)

    ordered_thresholds = []
    ordered_tweets_numbers = []
    for file_name in training_files:
        ordered_thresholds.append(thresholds.get(file_name, 0))
        ordered_tweets_numbers.append(Reader.count_tweets(file_name))
    train(ordered_tweets_numbers, {file_name: Reader.count_tweets(file_name) for file_name in test_files},
          ordered_thresholds, args.output, args.v)
