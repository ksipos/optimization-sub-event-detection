import time
from collections import deque
from os.path import basename, join, splitext

from BurstEventDetector import BurstEventDetector
from Period import Period
from ReadData import Reader
from SummaryWriter import SummaryWriter


def start(args):
    script_time = time.time()
    reader = Reader()
    file_name = splitext(basename(args.input))[0]
    results_file = join(args.output, file_name + ".txt")
    summary_file = join(args.output, file_name + "_summary.txt")
    open(results_file, 'w').close()
    open(summary_file, 'w').close()
    print "Starting the processing of file:", file_name
    iterations = 0
    periods_window = deque(maxlen=args.periods)
    summary, summary_index = [], []
    while True:
        iterations += 1
        start_time = time.time()
        tweets = reader.get_tweets(args.input)
        if tweets is None:
            break
        elif len(tweets) == 0:
            if args.v:
                print "Time period", iterations, "was skipped because there was 0 tweets published."
            continue
        if args.v:
            print "#" * 25, "Time period", iterations, "#" * 25

        if args.burst:
            period = BurstEventDetector(tweets, results_file, args.v)
            event = period.detect_event()
        else:
            period = Period(tweets, results_file, args.v)
            event = period.detect_event(periods_window)
        if event[1]:
            summary.append(event[2])
            summary_index.append(iterations)
        periods_window.append(period)
        if args.v:
            print "Time period #", iterations, ", Completion time:", time.time() - start_time
    SummaryWriter.write_summary(summary_file, summary, summary_index)

    print "Total time:", time.time() - script_time
