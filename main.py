from argparse import ArgumentParser
from csv import reader
from os.path import basename, isdir, isfile, splitext
from sys import stderr
from time import time

import Settings
from detection_process import start


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("input", help="path to the file containing the tweets that will be used as input", type=str)
    parser.add_argument("--output", help="path to the directory where the results would be stored", default="./output",
                        type=str)
    parser.add_argument("--threshold",
                        help="the threshold that will be used to determine if a period contains a sub-event",
                        default=0, type=float)
    parser.add_argument("--periods", help="number of previous periods that are considered (default 10)", default=10,
                        type=int)
    parser.add_argument("--duration", help="duration of the period set in minutes (default 1 min)", default=1,
                        type=float)

    parser.add_argument("--urls", help="do not remove urls during preprocessing", action="store_false")
    parser.add_argument("--retweets", help="do not remove retweets during preprocessing", action="store_false")
    parser.add_argument("--mentions", help="do not remove users' mentions during preprocessing", action="store_false")
    parser.add_argument("--stemming", help="do not perform stemming during preprocessing", action="store_false")
    parser.add_argument("--duplicates", help="do not remove duplicate tweets during preprocessing",
                        action="store_false")

    parser.add_argument("--burst", help="use the burst detector instead of the optimized method", action="store_true")
    parser.add_argument("--v", help="increase output verbosity", action="store_true")

    parser.add_argument("--summary", help="number of tweets that the summary will contains (default 2)", default=2,
                        type=int)
    parser.add_argument("--sp", help="number of previous periods where the reappearance of an sub-event is considered "
                                     "duplicate (default 3)", default=3, type=int)
    parser.add_argument("--c", help="cosine similarity threshold to compare the summaries (default 0.6)", default=0.6,
                        type=float)

    return parser.parse_args()


def argument_check():
    args = parse_arguments()
    if not isfile(args.input):
        stderr.write('The specified input file ' + str(args.input) + ' was not found.')
        raise SystemExit(1)
    elif not isdir(args.output):
        stderr.write('The specified parameter should be a directory name, not a file.')
        raise SystemExit(1)
    elif args.duration <= 0:
        stderr.write('The duration of the period should be a non-negative number.')
        raise SystemExit(1)
    elif args.periods <= 0:
        stderr.write('The number of periods should be a non-negative number.')
        raise SystemExit(1)
    elif args.summary <= 0:
        stderr.write('The number of tweets should be a non-negative number.')
        raise SystemExit(1)
    elif args.sp <= 0:
        stderr.write('The number of periods should be a non-negative number.')
        raise SystemExit(1)
    elif not (0 <= args.c <= 1):
        stderr.write('The cosine similarity threshold should belong in range [0, 1].')
        raise SystemExit(1)
    elif args.threshold < 0:
        stderr.write('The threshold should be a non-negative number.')
        raise SystemExit(1)
    return args


def set_parameters(args):
    Settings.TIME_WINDOW = args.duration * 60000
    Settings.IGNORE_URLS = args.urls
    Settings.IGNORE_RETWEETS = args.retweets
    Settings.IGNORE_DUPLICATES = args.duplicates
    Settings.IGNORE_USERNAME = args.mentions
    Settings.STEMMING = args.stemming
    Settings.TWEETS_SUMMARY = args.summary
    Settings.SUMMARY_SIMILARITY = args.c
    Settings.SUMMARY_PERIODS = args.sp
    Settings.PREVIOUS_PERIODS = args.periods

    if args.burst:
        Settings.BURST_DETECTION_THRESHOLD[splitext(args.input)] = args.threshold
    else:
        Settings.EVENT_DETECTION_THRESHOLD[basename(args.input)] = args.threshold

    with open(args.input, 'rb') as f:
        for row in reader(f):
            Settings.EVENT_TIMESTAMPS[basename(args.input)] = [int(row[0]), int(round(time() * 1000))]
            break


if __name__ == "__main__":
    args = argument_check()
    set_parameters(args)
    start(args)
