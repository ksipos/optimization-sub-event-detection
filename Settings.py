###################################### Timestamp parameters ######################################
EVENT_TIMESTAMPS = {}

###################################### Preprocessing parameters ######################################
IGNORE_URLS = True
IGNORE_RETWEETS = True
IGNORE_DUPLICATES = True
IGNORE_USERNAME = True
STEMMING = True
TIME_WINDOW = -1

###################################### Event detection parameters ######################################
PREVIOUS_PERIODS = -1

EVENT_DETECTION_THRESHOLD = {}

BURST_DETECTION_THRESHOLD = {}
###################################### Output parameters ######################################
TWEETS_SUMMARY = -1
SUMMARY_SIMILARITY = -1
SUMMARY_PERIODS = -1
