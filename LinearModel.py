import numpy as np
import matplotlib.pyplot as plt


class LinearModel:
    def __init__(self):
        self.slope = -1
        self.intercept = -1
        self.power = -1

    @staticmethod
    def find_line(xd, yd, order=1, plot=False):
        """Make a line of best fit"""

        # Calculate trendline
        coeffs = np.polyfit(xd, yd, order)

        intercept = coeffs[-1]
        slope = coeffs[-2]
        power = coeffs[0] if order == 2 else 0

        minxd = np.min(xd)
        maxxd = np.max(xd)

        xl = np.array([minxd, maxxd])
        yl = power * xl ** 2 + slope * xl + intercept

        if plot:
            plt.plot(xl, yl, 'r', alpha=1)
            plt.scatter(xd, yd)
            plt.show()
        return slope, intercept, power

    def fit(self, x, y, order=1):
        self.slope, self.intercept, self.power = LinearModel.find_line(x, y, order)

    def predict(self, x):
        return self.power * x ** 2 + self.slope * x + self.intercept

    @staticmethod
    def get_prediction(x, slope, intercept, power):
        return power * x ** 2 + slope * x + intercept

    def get_parameters(self):
        return self.slope, self.intercept, self.power
