import bisect
import re

from nltk.stem import *

import Settings


class Preprocessor:
    """Class that is used to preprocess the input tweets"""

    def __init__(self):
        pass

    stopwords = []
    stemmer = None

    @staticmethod
    def contains_url(tweet):
        """Method that is used to check if a tweet contains a url"""
        return len(re.findall(r"http[s]?\S+", tweet)) != 0

    @staticmethod
    def is_retweet(tweet):
        """Method that is used to check if a tweet is retweet"""
        return len(re.findall('rt @?[a-zA-Z0-9_]+:? .*', tweet)) != 0

    @staticmethod
    def contains_username(tweet):
        """Method that is used to check if a tweet contains the character "@", therefore refers to a user"""
        return '@' in tweet

    @classmethod
    def preprocess(cls, tweets):
        """Method that is used to preprocess the incoming tweet. Returns a list with the preprocessed tweets, the full
        text of the tweets that are returned, the corresponding timestamps and the vocabulary of all the tweets"""
        if not cls.stopwords:
            cls.stemmer = PorterStemmer()
            cls.stopwords = [re.sub('[\s+]', ' ', word.decode("utf-8-sig").encode("utf-8")).strip() for word in
                             open("stop_words.txt", 'rb')]
        full_tweets, timestamps, result = [], [], []
        unique_words_index = set()
        for tweet in tweets:
            timestamp = tweet[0]
            tweet = tweet[1].lower()  # Keep the text part
            if (Settings.IGNORE_URLS and cls.contains_url(tweet)) or \
                    (Settings.IGNORE_RETWEETS and cls.is_retweet(tweet)) or \
                    (Settings.IGNORE_USERNAME and cls.contains_username(tweet)):
                continue
            if not Settings.IGNORE_URLS and cls.contains_url(tweet):
                tweet = re.sub(r"http[s]?\S+", "", tweet)
            if not Settings.IGNORE_RETWEETS and cls.is_retweet(tweet):
                tweet = re.sub('rt @?[a-zA-Z0-9_]+:?', '', tweet)
            if not Settings.IGNORE_USERNAME and cls.contains_username(tweet):
                tweet = re.sub('@[a-zA-Z0-9_]+:?', '', tweet)

            tweet = re.sub(r'\W+', ' ', tweet)  # Remove special characters
            tweet = re.sub('[\s+]', ' ', tweet).strip()  # Remove spaces and new lines

            full_tweet = tweet

            tweet = [word for word in tweet.split(" ") if cls.stopwords[bisect.bisect(cls.stopwords, word) - 1] != word]
            if Settings.STEMMING:
                tweet = [cls.stemmer.stem(word) for word in tweet]

            if not Settings.IGNORE_DUPLICATES or tweet not in result:
                unique_words_index.update(tweet)
                result.append(tweet)
                full_tweets.append(full_tweet)
                timestamps.append(timestamp)
        return result, full_tweets, timestamps, sorted(unique_words_index), [tweets[0][1], tweets[-1][1]]
