class Util:
    """Class that is used to gather utility methods"""
    def __init__(self):
        pass

    @staticmethod
    def beauty_print(to_print):
        """Method that is used to "beautify" a input list"""
        if not to_print:
            return []
        max_len = len(max(to_print, key=len))
        result = "*" * (max_len + 4) + "\n"
        for i in range(len(to_print)):
            result += "* " + to_print[i].center(max_len) + " *" + "\n"
        result += "*" * (max_len + 4) + "\n"
        return str(result)
