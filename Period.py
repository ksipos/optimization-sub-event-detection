import bisect

import numpy as np
from cvxpy import Variable, Minimize, Problem, norm
from scipy.sparse import find

import Settings
from Preprocessor import Preprocessor
from Util import Util


class Period:
    """Class that is used to represent a time period and to detect sub-events inside it"""

    file_name = ""

    def __init__(self, tweets, file_name, printing=True, write=True):
        self.tweets_edges = []
        self.adjacency_matrix = None
        tweets, self.full_tweets, self.tweets_timestamps, self.vocabulary, self.timestamps = Preprocessor.preprocess(
            tweets)

        self.tweets_number = len(tweets)
        self.generate_adjacency_matrix_dense(tweets)
        self.toPrint = ["Period starts: " + self.timestamps[0] + " Period ends: " + self.timestamps[1],
                        "Number of tweets: " + str(self.tweets_number),
                        "Vocabulary size: " + str(len(self.vocabulary))]
        self.write = write
        self.printing = printing
        Period.file_name = file_name

    def generate_adjacency_matrix_dense(self, tweets):
        """Method that is used to generate the adjacency matrix of the given tweets"""
        wordsNumber = len(self.vocabulary)
        adjacency_matrix = np.zeros((wordsNumber, wordsNumber))
        tweet_counter = -1
        for tweet in tweets:
            tweet = set(tweet)  # Duplicates
            indexes = [bisect.bisect_left(self.vocabulary, word) for word in tweet]
            counter = 0
            tweet_counter += 1
            self.tweets_edges.append([])
            for i in indexes:
                for j in indexes[counter:]:
                    if i == j:
                        continue
                    adjacency_matrix[i, j] += 1.0 / len(tweet)
                    adjacency_matrix[j, i] += 1.0 / len(tweet)
                    self.tweets_edges[tweet_counter].append(sorted([self.vocabulary[i], self.vocabulary[j]]))
                counter += 1
        self.adjacency_matrix = adjacency_matrix

    def get_edges_weight(self, edges_list, nodes_list):
        """Method that is used to extract the weight for each edge in the given list. The nodes_list parameter is a
        list that contains the nodes that are included in the given edges """
        nodes = {}
        for node in nodes_list:
            index = bisect.bisect(self.vocabulary, node) - 1
            if (0 <= index <= len(self.vocabulary)) and self.vocabulary[index] == node:
                nodes[node] = index

        weight_list = []
        for edge in edges_list:
            first_word, second_word = edge[0], edge[1]
            if all(word in nodes for word in (first_word, second_word)):
                indexes = [nodes[first_word], nodes[second_word]]
                indexes.sort()
                weight_list.append(self.adjacency_matrix[indexes[0], indexes[1]])
            else:
                weight_list.append(0)
        return weight_list

    @staticmethod
    def get_nonzero_edges(matrix):
        """Method that is used to extract from the adjacency matrix the edges with no-negative weights"""
        rows, columns, values = find(matrix)
        return [[rows[i], columns[i], float(values[i])] for i in range(len(rows))]

    def generate_vector(self):
        """Method that is used to generate a vector for the current period"""
        non_zero_edges = self.get_nonzero_edges(self.adjacency_matrix)
        vector = np.zeros((len(non_zero_edges), 1))
        vector_edges = []
        vector_nodes = set()
        weighted_edges = {}
        counter = 0
        for row, column, value in non_zero_edges:
            vector[counter] = value
            nodes = [self.vocabulary[row], self.vocabulary[column]]
            vector_edges.append(nodes)
            vector_nodes.update(nodes)
            weighted_edges[tuple(sorted(nodes))] = value
            counter += 1
        return vector, vector_nodes, vector_edges, weighted_edges

    def detect_event(self, previous_periods):
        """Method that is used to decide if the current period is an event by taking advantage of the Least Squares
        Optimization """
        if self.tweets_number == 0:
            return [[], False, "No tweets found in the current period."]
        period_score = -1
        vector, vector_nodes, vector_edges, weighted_edges = self.generate_vector()
        repr_tweets = self.submodular_tweets(weighted_edges, [], [], Settings.TWEETS_SUMMARY)
        if len(previous_periods) != 0:
            weights = np.zeros((len(vector_edges), len(previous_periods)))
            for i in range(len(previous_periods)):
                weights[:, i] = np.asarray(previous_periods[i].get_edges_weight(vector_edges, vector_nodes))

            period_score = Period.optimize(weights, vector)

        is_event = period_score >= Settings.EVENT_DETECTION_THRESHOLD[Period.file_name.split("/")[-1][:-4] + ".csv"]
        self.toPrint += [" "] + repr_tweets + [" "] + ["Result: " + str(is_event) + " " + str(period_score)]
        if self.printing:
            print Util.beauty_print(self.toPrint)

        if self.write:
            f = open(Period.file_name, 'a')
            f.write(Util.beauty_print(["Timestamp: " + ' '.join(self.timestamps)] + repr_tweets +
                                      ["-------------------------------------------"] +
                                      ["Result: " + str(is_event)] +
                                      ["Score: " + str(period_score)]))
            f.close()
        timestamps = self.timestamps
        del self.full_tweets
        del self.timestamps
        del self.toPrint
        del self.tweets_edges
        del self.tweets_number
        del self.tweets_timestamps
        return [timestamps, is_event, ". ".join(repr_tweets) + " || " + str(period_score)]

    @staticmethod
    def optimize(A, b):
        """Method that solves the Least Squares problem"""
        x = Variable(A.shape[1])
        objective = Minimize(norm(A * x - b))  # Minimize(norm(A * x - b) + lamp * norm(x, p=1))
        constraints = [0 <= x, sum(x) == 1]
        minimum = Problem(objective, constraints).solve()
        value = A.dot(x.value) - b
        value[value > 0] = 0
        minimum = np.linalg.norm(value)
        return minimum

    def submodular_tweets(self, weighted_edges, excluded_tweets, excluded_edges, tweets_number):
        """Method that is used to generate a summary for the current period"""
        if tweets_number == 0:
            return []
        tweets_ranking = [0] * self.tweets_number
        for tweet_counter in range(len(self.tweets_edges)):
            if tweet_counter in excluded_tweets:
                continue
            tweet_edges = self.tweets_edges[tweet_counter]
            tweet_value = 0
            for edge in tweet_edges:
                if tuple(edge) not in excluded_edges:
                    tweet_value += weighted_edges[tuple(edge)]
            tweets_ranking[tweet_counter] = tweet_value
        sorted_indexes = sorted(range(len(tweets_ranking)), key=lambda i: tweets_ranking[i], reverse=True)
        tweet_index = sorted_indexes[:Settings.TWEETS_SUMMARY][0]
        excluded_tweets.append(tweet_index)
        excluded_edges += [tuple(edge) for edge in self.tweets_edges[tweet_index]]
        return [self.full_tweets[tweet_index]] + self.submodular_tweets(weighted_edges, excluded_tweets, excluded_edges,
                                                                        tweets_number - 1)
